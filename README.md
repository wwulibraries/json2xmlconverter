# README #

WWU uses this to create JSON needed to created MODS for Islandora.

Here is what it looks like:
![screenshot of this web application](public/images/screenshot.png)


### What is this repository for? ###

* This web application provides an almost real-time tool for converting JSON to XML using the npm module js2xmlparser.

### How do I get set up? ###

* clone / download the repo
* cd into the directory
* npm install
* DEBUG=json2xml:* nodemon
* visit the app in your browser @ http://localhost:3000/

### Who do I talk to? ###

* david dot bass at wwu dot edu
