$(document).ready(function() {

    let changeOn = 'change keyup paste'; 

    getUpdatePreference = function() {
        let onTabChecked = $('#updateOnTab').is(":checked");
        if (onTabChecked) {
            changeOn = 'change';
         } else {
            changeOn = 'change keyup paste'; 
         }
        //  console.log(onTabChecked, changeOn);
         return changeOn;
    };

    $('input[type=radio][name=updateOn]').on('change', function() {
        // console.log('updated radio');
        changeOn = getUpdatePreference();
    });

    $('#data').on(changeOn, function(e) {
        if (changeOn.includes(e.type)) {
            // // erase the xml, in case the new request fails (to show that its invalid)
            $('#xml').val('');

            var jsonData = document.getElementById('data').value;
            
            $.ajax({ 
                type: "POST",
                url: "/update", 
                data: jsonData,
                contentType: 'application/json', 
                dataType: 'json',
            }) 
            .fail(function( response ) {
                // console.log('fail', response);
            })
            .done(function( response ) {
                // console.log('done', response);
                $( "#xml" ).val( response.xmlData );
            });
        }
    });
});
