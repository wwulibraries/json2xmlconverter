// David Bass @ WWU 
// June 2018
// Purpose: create a web interface for converting JSON to XML in real time
// License: MIT

var express = require('express');
var router = express.Router();
var js2xmlparser = require("js2xmlparser");

function generateXML(jsonInput, res, next, update) {
  var MODS = '';
  var jsonString =JSON.stringify(jsonInput, null, 2);


  var inputType = typeof(jsonInput);  
  // console.log('inputType', inputType, 'update', update, '-----------------------------');
  if (inputType === 'object') {

    let modsAttributes ={ 
      "@": {
        "xmlns:xlink":"http://www.w3.org/1999/xlink", 
        "xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance", 
        "xmlns":"http://www.loc.gov/mods/v3", 
        "xsi:schemaLocation":"http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-6.xsd"
      }, 
    };
    let newJson = Object.assign(modsAttributes, jsonInput);

    var options = {
      declaration: {
          include: true,
          encoding: "UTF-8",
        },
      format: {
          doubleQuotes: true,
          indent: "\   ",
          newline: "\r\n",
          pretty: true
      },
  };

    // console.log(jsonInput);
    if (update) {
      MODS = js2xmlparser.parse("mods", newJson, options);
      res.json({ xmlData: MODS }); 
    } else {
      MODS = js2xmlparser.parse("mods", newJson, options);
      res.render('index', { jsonInput: jsonString, xmlData: MODS }); 
    }
    res.end();
  }
}

router.get('/', function(req, res, next) {
  var jsonData = require('../data.json');
  var update = false;
  generateXML(jsonData, res, next, update);
});

router.post('/update', function(req, res, next) {
  var jsonData = req.body;
  var update = true;
  generateXML(jsonData, res, next, update);
});

module.exports = router;
